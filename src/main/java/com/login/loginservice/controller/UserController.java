package com.login.loginservice.controller;

import com.login.loginservice.model.User;
import com.login.loginservice.model.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;


@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = POST, value = "/adduser")
    public void addUserController(@RequestBody User user) {
        userService.addUser(user);
    }

    @RequestMapping(value="/login", method = POST)
    public void loginUserController(@RequestBody User user) {
        userService.loginUser(user);
    }

    @RequestMapping(value="/change/{variable}/{value}", method = PUT)
    public void changeValueController(@RequestBody User user, @PathVariable String variable, @PathVariable String value) {
        userService.changeValue(user, variable, value);
    }

    @RequestMapping(value="/block/{username}", method = PUT)
    public void blockUserController(@PathVariable String username) {
        userService.blockUser(username);
    }
}
