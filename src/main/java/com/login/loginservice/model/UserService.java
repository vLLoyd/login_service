package com.login.loginservice.model;

import com.login.loginservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public void addUser(User user) {
        User checkUser = userRepository.findByUsername(user.getUsername());
        if (checkUser != null) {
            System.out.println("User " + user.getUsername() + " already exists");
        }
        userRepository.save(user);
        System.out.println("User " + user.getUsername() + " was added");
    }

    public void loginUser(User user) {
        User checkUser = userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());
        if(checkUser != null){
            if(!checkUser.getIsBlocked()) {
                System.out.println("User " + user.getUsername() + " is logged in");
            }
            System.out.println("User " + user.getUsername() + " is blocked");
        }
        System.out.println("User " + user.getUsername() + " not found");
    }

    public void changeValue(User user, String variable, String value) {
        User changedUser = userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());
        if(changedUser != null) {
            if(!changedUser.getIsBlocked()) {
                switch (variable) {
                    case ("username"): changedUser.setUsername(value);
                    case ("password"): changedUser.setPassword(value);
                    default:
                        System.out.println("There is no such field in database or it is unchangeable");
                }
                userRepository.save(changedUser);
                System.out.println("User`s " + user.getUsername() + " " + variable + " was changed to " + value);
            }
            System.out.println("User " + user.getUsername() + " is blocked");
        }
        System.out.println("User " + user.getUsername() + " not found");
    }

    public void blockUser(String username) {
        User blockedUser = userRepository.findByUsername(username);

        if (blockedUser != null) {
            if (!blockedUser.getIsBlocked()) {
                blockedUser.setIsBlocked(true);
                userRepository.save(blockedUser);
                System.out.println("User " + username + " was successfully blocked");
            }
            System.out.println("User " + username + " is already blocked");
        }
        System.out.println("User " + username + " not found");
    }
}
